export function isValidPassword (password) {
    const upperRegex = /[A-Z]+/;
    const numberRegex = /[0-9]+/;
    const specialRegex = /[^0-9a-zA-Z]+/;

    return password.length > 7 && upperRegex.test(password) === true && numberRegex.test(password) === true &&
        specialRegex.test(password) === true;
}
