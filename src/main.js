import Vue from 'vue'
import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-sugar.css';
import axios from 'axios';
import $ from 'jquery';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import store from '@/store';
import router from '@/router';
import App from '@/App.vue';

window.$ = $;

axios.defaults.baseURL = 'http://localhost';
axios.defaults.withCredentials = true;
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

Vue.config.productionTip = false;

Vue.use(VueToast);

// Get cookies
axios.get('/sanctum/csrf-cookie');

new Vue({
    router: router,
    store: store,
    render: h => h(App),
}).$mount('#app')
