import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '@/components/Home';
import Register from '@/components/Register';
import Login from '@/components/Login';
import ForgotPassword from '@/components/ForgotPassword';
import ResetPassword from '@/components/ResetPassword';
import store from '@/store';

Vue.use(VueRouter);

const routes = [
    {path: '/', component: Home, meta: {requiresAuth: true}},
    {path: '/register', component: Register},
    {path: '/login', component: Login},
    {path: '/forgot-password', component: ForgotPassword},
    {path: '/reset-password/:token', component: ResetPassword}
];

const router = new VueRouter({
    mode: 'history',
    linkActiveClass: 'active',
    routes
});

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (store.state.auth.isAuthenticated !== true) {
            next('/login');
            return;
        }
    }

    next();
});

export default router;
