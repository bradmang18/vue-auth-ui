import Vue from 'vue';
import Vuex from 'vuex';
import app from '@/store/modules/app';
import auth from '@/store/modules/auth';
import forgotPassword from '@/store/modules/forgot-password';
import register from '@/store/modules/register';
import resetPassword from '@/store/modules/reset-password';

Vue.use(Vuex);

export default new Vuex.Store({
    strict: process.env.NODE_ENV !== 'production',
    modules: {
        app,
        auth,
        forgotPassword,
        register,
        resetPassword
    }
});
