export default {
    namespaced: true,
    state: {
        error: '',
        success: '',
        loading: ''
    },
    mutations: {
        setError (state, {message}) {
            state.error = message;
        },
        clearError (state) {
            state.error = '';
        },
        setSuccess (state, {message}) {
            state.success = message;
        },
        clearSuccess (state) {
            state.success = '';
        },
        setLoading (state, {message}) {
            state.loading = message;
        },
        clearLoading (state) {
            state.loading = '';
        }
    },
    getters: {
        isError (state) {
            return state.error !== '';
        },
        isSuccess (state) {
            return state.success !== '';
        },
        isLoading (state) {
            return state.loading !== '';
        }
    }
}
