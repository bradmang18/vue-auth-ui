import axios from 'axios';
import router from '@/router';

export default {
    namespaced: true,
    state: {
        email: '',
        password: '',
        isAuthenticated: false,
        user: null
    },
    getters: {
        isFormValid (state) {
            return state.email !== '' && state.password !== '';
        }
    },
    mutations: {
        updateEmail (state, {email}) {
            state.email = email;
        },
        updatePassword (state, {password}) {
            state.password = password;
        },
        authenticate (state, {user}) {
            state.isAuthenticated = true;
            state.user = user;
        },
        unauthenticate (state) {
            state.isAuthenticated = false;
            state.user = null;
        },
        clearForm (state) {
            state.email = '';
            state.password = '';
        }
    },
    actions: {
        async login ({commit, state}) {
            try {
                await axios.post('/login', {
                    email: state.email,
                    password: state.password
                });

                const response = await axios.get('/api/users/current')

                commit('authenticate', {
                    user: response.data
                });

                commit('clearForm');
                await router.push('/');

            } catch (error) {
                if (error.response && error.response.status === 422) {
                    commit('app/setError', {message: 'Invalid credentials'}, {root: true});

                } else {
                    commit('app/setError', {message: 'Unable to log in'}, {root: true});
                }
            }
        },
        async logout ({commit}) {
            try {
                await axios.post('/logout');

                commit('unauthenticate');
                await router.push('/login');

            } catch (error) {
                alert('Unable to log out');
            }
        }
    }
};
