import axios from 'axios';
import router from '@/router';
import {isValidPassword} from '@/utilities/validation';

export default {
    namespaced: true,
    state: {
        firstName: '',
        lastName: '',
        email: '',
        password: '',
        passwordConfirmation: '',
        errors: {}
    },
    getters: {
        isFormValid (state) {
            return state.firstName !== '' && state.lastName !== '' && state.email !== '' &&
                isValidPassword(state.password) && state.password === state.passwordConfirmation;
        }
    },
    mutations: {
        updateFirstName (state, {firstName}) {
            state.firstName = firstName;
        },
        updateLastName (state, {lastName}) {
            state.lastName = lastName;
        },
        updateEmail (state, {email}) {
            state.email = email;
        },
        updatePassword (state, {password}) {
            state.password = password;
        },
        updatePasswordConfirmation (state, {passwordConfirmation}) {
            state.passwordConfirmation = passwordConfirmation;
        },
        setErrors (state, {errors}) {
            state.errors = errors;
        },
        clearErrors (state) {
            state.errors = {};
        },
        clearForm (state) {
            state.firstName = '';
            state.lastName = '';
            state.email = '';
            state.password = '';
            state.passwordConfirmation = '';
        }
    },
    actions: {
        async register ({commit, state, dispatch}) {
            commit('clearErrors');

            try {
                await axios.post('/register', {
                    first_name: state.firstName,
                    last_name: state.lastName,
                    email: state.email,
                    password: state.password,
                    password_confirmation: state.passwordConfirmation
                });

                dispatch('resetForm');
                commit('app/setSuccess', {message: 'Successfully registered. Please log in.'}, {root: true});
                await router.push('/login');

            } catch (error) {
                console.log(error);
                if (error.response && error.response.status === 422) {
                    commit('setErrors', {errors: error.response.data.errors});

                } else {
                    commit('app/setError', {message: 'An error occurred during registration'}, {root: true});
                }
            }
        },
        resetForm({commit}) {
            commit('clearForm');
            commit('clearErrors');
        }
    }
};
