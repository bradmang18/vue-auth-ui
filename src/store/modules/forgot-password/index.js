import axios from 'axios';
import router from '@/router';

export default {
    namespaced: true,
    state: {
        email: '',
        errors: {}
    },
    getters: {
        isFormValid (state) {
            return state.email !== '';
        }
    },
    mutations: {
        updateEmail (state, {email}) {
            state.email = email;
        },
        clearForm (state) {
            state.email = '';
        },
        setErrors (state, {errors}) {
            state.errors = errors;
        },
        clearErrors (state) {
            state.errors = {};
        }
    },
    actions: {
        async submit ({commit, state, dispatch}) {
            commit('clearErrors');

            try {
                await axios.post('/forgot-password', {
                    email: state.email
                });

                commit('app/setSuccess',
                    {message: 'An email has been sent with instructions for resetting your password.'}, {root: true});

                dispatch('resetForm');
                await router.push('/login');

            } catch (error) {
                console.log(error);
                if (error.response && error.response.status === 422) {
                    commit('setErrors', {errors: error.response.data.errors});

                } else {
                    commit('app/setError', {message: 'Unable to submit password reset request'}, {root: true});
                }
            }
        },
        resetForm({commit}) {
            commit('clearForm');
            commit('clearErrors');
        }
    }
}
