import axios from 'axios';
import router from '@/router';
import {isValidPassword} from '@/utilities/validation';

export default {
    namespaced: true,
    state: {
        token: '',
        email: '',
        password: '',
        passwordConfirmation: '',
        errors: {}
    },
    getters: {
        isFormValid (state) {
            return state.email !== '' && isValidPassword(state.password) &&
                state.password === state.passwordConfirmation;
        }
    },
    mutations: {
        updateEmail (state, {email}) {
            state.email = email;
        },
        updatePassword (state, {password}) {
            state.password = password;
        },
        updatePasswordConfirmation(state, {passwordConfirmation}) {
            state.passwordConfirmation = passwordConfirmation;
        },
        setToken (state, {token}) {
            state.token = token;
        },
        clearForm (state) {
            state.token = '';
            state.email = '';
            state.password = '';
            state.passwordConfirmation = '';
        },
        setErrors (state, {errors}) {
            state.errors = errors;
        },
        clearErrors (state) {
            state.errors = {};
        }
    },
    actions: {
        async resetPassword ({commit, state, dispatch}) {
            commit('clearErrors');

            try {
                await axios.post('reset-password', {
                    token: state.token,
                    email: state.email,
                    password: state.password,
                    password_confirmation: state.passwordConfirmation
                });

                dispatch('resetForm');
                commit('app/setSuccess', {message: 'Successfully reset password. Please log in.'}, {root: true});
                await router.push('/login');

            } catch (error) {
                if (error.response && error.response.status === 422) {
                    commit('setErrors', {errors: error.response.data.errors});

                } else {
                    commit('app/setError', {message: 'Unable to reset password'}, {root: true});
                }
            }
        },
        resetForm ({commit}) {
            commit('clearForm');
            commit('clearErrors');
        }
    }
}
